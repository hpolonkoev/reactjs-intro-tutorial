# Change Log
All changes of ReactJS intro tutorial will be documented in this file

<hr><br>

## 28/05/2019
Init project 

### Init

- run installation to create react app `npx create-react-app reactjs-intro-tutorial`

### Added

- add gitignore file

### Updated

- index.js and index.css from /src file with the tutorial code (tic-tac-toc)

### Removed

- remove all files from /src folder
